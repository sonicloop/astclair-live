/* AStClair App
   SonicLoop
*/


angular.module('AStClair', []).config(function ($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});


angular.module('AStClair').controller('CatalogueCtrl', function ($scope, $http) {
    $scope.ver = 'AStClair v1';
    $scope.slides = [];
    $scope.bodyS = "all";
    $scope.lengthS = "all";
    $scope.cutS = "all";

    // Does not work due to CORS var jsonUrl = 'https://d3e025498cf91f480bb2daf5fa4bec6a:ccff8de526d6a5c8e202ed5ed4baa3c9@a-st-clair-2.myshopify.com/admin/products.json?callback=handler';
    var jsonUrl = '/products.json';  
  $http.get(jsonUrl).
      success(function(data, status, headers, config) {
        console.log("success", data);
        for (var i = data.products.length - 1; i >= 0; i--) {
            var cut = "none";
            var body = "none";
            var length = "none";
          
            if(data.products[i].handle.indexOf("v-neck") >= 0){
                 cut = ["all","v-neck"];
                 }
            else if(data.products[i].handle.indexOf("scoop") >= 0){
                 cut = ["all","scoop"];
                 }
            else if(data.products[i].handle.indexOf("crew") >= 0){
                 cut = ["all","crew"];
                 } ; 
          
            if(data.products[i].title.indexOf("Tee") >= 0){
                 body = ["all","Tee"];
                 }
            else if(data.products[i].title.indexOf("Tank") >= 0){
                 body = ["all","Tank"];
                 }

            if(data.products[i].title.indexOf("Regular") >= 0){
                 length = ["all","Regular"];
                 }
            else if(data.products[i].title.indexOf("Long") >= 0){
                 length = ["all","Long"];
                 }
          
            if(cut !== "none" && body !== "none" && length !== "none" ){
              var product = {
               'image_src' : data.products[i].images[0].src,
               'title1' :  data.products[i].title,
               'desc' : data.products[i].body_html,
               'link' : '/products/'+ data.products[i].handle,
                'cut' : cut,
                'body' : body,
                'length' : length
               };
              if(i===0){
                product.class = 'flex-active-slide';
                product.style = {'width': '100%', 
                              'float': 'left',
                              'margin-right': '-100%',
                              'position': 'relative',
                              'opacity': '1',
                              'display': 'block', 
                              'z-index': '2',
                              'visibility': 'visible',
					          'transition': 'opacity 1s linear'
                             };
              } else {
                product.class = '';
                product.style = {'width': '100%', 
                              'float': 'left',
                              'margin-right': '-100%',
                              'position': 'relative',
                              'opacity': '0',
                              'display': 'block', 
                              'z-index': '1',
                              'visibility': 'hidden',
					          'transition': 'visibility 0s 1s, opacity 1s linear'
                             };
              }
              $scope.slides.push(product);
            }
            
              
        };
        console.log($scope.slides);    

      }).
      error(function(data, status, headers, config) {
        console.log(config, status);
      });
  
  var resetSlides = function(){
    var slides = $scope.filteredSlides;
    var reset = true;
    var count = 0;
    for (var i = slides.length - 1; i >= 0; i--) {
      if(slides[i].class === "flex-active-slide"){
        reset = false;
        count++;
      }
    }
    if((reset || count > 1) && slides.length >= 1){
      slides[0].class = "flex-active-slide";
      slides[0].style = {'width': '100%', 
                              'float': 'left',
                              'margin-right': '-100%',
                              'position': 'relative',
                              'opacity': '1',
                              'display': 'block', 
                              'z-index': '2',
                              'visibility': 'visible',
					          'transition': 'opacity 1s linear'
                             };
      $('#nextRight').click();
      
    }
  }
  setInterval(function(){ resetSlides();}, 100);
  
  $scope.updateSlides = function(change){
    var slides = $scope.filteredSlides;
    var next = -1;
    for (var i = slides.length - 1; i >= 0; i--) {
      if(slides[i].class === "flex-active-slide"){
        next = i + change;
        if(next < 0) {
          next = slides.length - 1;
        };
        if(next > slides.length - 1){
          next = 0;
        }
        
      }
      console.log(i, slides[i].class);
        slides[i].class = "";
        slides[i].style = {'width': '100%', 
                              'float': 'left',
                              'margin-right': '-100%',
                              'position': 'relative',
                              'opacity': '0',
                              'display': 'block', 
                              'z-index': '1',
                               'visibility': 'hidden',
					          'transition': 'visibility 0s 1s, opacity 1s linear'
                             };
        
      
    }
    console.log(slides);
    if(next >= 0){
      slides[next].class = "flex-active-slide";
      slides[next].style = {'width': '100%', 
                              'float': 'left',
                              'margin-right': '-100%',
                              'position': 'relative',
                              'opacity': '1',
                              'display': 'block', 
                              'z-index': '2',
                            'visibility': 'visible',
					          'transition': 'opacity 1s linear'
                             };
    }
    return;
  }
  
});
